import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { v4 } from 'uuid';
import {
  Button,
  Select,
  MenuItem,
  TextField,
  Modal,
  Fade,
  Backdrop,
  FormControl,
  InputLabel,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import withLoader from '../Hoc/withLoader';

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}));

function Controls({ settings, setGameMode, gameStarted, gameFinished }) {
  const [ name, setName ] = useState('');
  const [ option, setOption ] = useState('');
  const [ open, setOpen ] = useState(false);
  const classes = useStyles();

  const handleClose = () => setOpen(false);

  const onStart = () => {
    if (name.length < 3) {
      return setOpen(true);
    }
    setGameMode(name, option || settings[0].name);
  };

  return (
    <>
      <div className="controls">
        <TextField
          label="Enter your name"
          disabled={gameStarted}
          value={name}
          onChange={({ target }) => setName(target.value.trim())}
        />
        <FormControl size="small" className={classes.formControl}>
          <InputLabel id="demo-simple-select-label">Mode</InputLabel>
          <Select
            disabled={gameStarted}
            value={option}
            onChange={({ target }) => setOption(target.value)}
          >
            {settings.map(({ name }) => (
              <MenuItem value={name} key={v4()}>{name}</MenuItem>
            ))}
          </Select>
        </FormControl>
        <Button
          disabled={gameStarted && !gameFinished}
          onClick={onStart}
          variant="contained"
          color="primary"
        >
          {gameFinished ? 'Play again' : 'Play'}
        </Button>
      </div>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className="modal"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className="paper">
            <h2 id="transition-modal-title">Your name has less than 3 symbols</h2>
          </div>
        </Fade>
      </Modal>
    </>
  );
}

Controls.propTypes = {
  settings: PropTypes.arrayOf(PropTypes.object),
  setGameMode: PropTypes.func,
  gameStarted: PropTypes.bool,
  gameFinished: PropTypes.bool,
};

Controls.defaultProps = {
  settings: [],
  setGameMode: () => {},
  gameStarted: false,
  gameFinished: false,
};

export default withLoader(Controls);
