import React from 'react';
import PropTypes from 'prop-types';
import { v4 } from 'uuid';
import Cell from '../Cell';

function Field({ cells, onCellClick, winner }) {
  const rowsCount = Math.sqrt(cells.length);
  const arr = [];

  cells.forEach((cell, i) => {
    const newLine = i && i % rowsCount === 0;
    if (newLine) {
      arr.push(<div className="break" key={v4()}/>);
    }
    arr.push(<Cell {...cell} onCellClick={onCellClick} key={cell.id} />);
  });

  return (
    <>
      {winner && <h1>The winner is {winner}</h1>}
      <div className="field">
        {arr}
      </div>
    </>
  );
}

Field.propTypes = {
  cells: PropTypes.arrayOf(PropTypes.object),
  onCellClick: PropTypes.func,
  winner: PropTypes.string,
};

Field.defaultProps = {
  cells: [],
  onCellClick: () => {},
  winner: '',
};

export default Field;
