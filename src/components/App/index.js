import React, { useState, useEffect } from 'react';
import { v4 } from 'uuid';
import { Grid, Container } from '@material-ui/core';
import moment from 'moment';
import Field from '../Field';
import LeaderBoard from '../LeaderBoard';
import Controls from '../Controls';
import { HUMAN, COMPUTER, API } from '../../constants';

const useRequest = (url) => {
  const [ response, setResponse ] = useState([]);
  const [ loading, setLoading ] = useState(true);
  
  const request = () => {
    fetch(`${API}${url}`)
      .then((res) => res.json())
      .then((res) => {
        if (Array.isArray(res)) {
          setLoading(false);
          setResponse(res);
        } else {
          const response = [];
          for (const key in res) {
            response.push({
              name: key,
              ...res[key],
            });
          }
          setLoading(false);
          setResponse(response);
        }
      })
      .catch((err) => console.log(err));
  };

  useEffect(request, []);

  return {
    response,
    loading,
    setResponse,
  };
};

const onMove = ({ cells }, randomId) => {
  return cells.map((cell) => {
    if (cell.id === randomId) {
      cell.isActive = true;
      cell.clickedBy = COMPUTER;
    } else {
      cell.isActive = false;
    }
    return cell;
  });
};

const postWinner = (winner, winners, setResponse) => {
  const date = moment().format('D MMM YYYY, HH:mm');
  const data = {
    winner,
    date,
  };

  fetch(`${API}/winners`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  })
    .then(({ ok }) => {
      if (ok) {
        setResponse([
          ...winners,
          {
            ...data,
            id: v4(),
          },
        ]);
      }
    });
};

function App() {
  const [ game, setGame ] = useState({});
  const { response: winners, loading: winnersLoading, setResponse } = useRequest('/winners');
  const { response: settings, loading: settingsLoading } = useRequest('/game-settings');

  const setGameMode = (userName, mode) => {
    const { field, delay } = settings.find(({ name }) => name === mode);
    const cells = new Array(field ** 2).fill({}).map(() => ({
      id: v4(),
      isActive: false,
      clickedBy: '',
    }));
    const unusedCells = cells.map(({ id }) => id);
    
    setGame({
      userName,
      delay,
      cells,
      randomId: '',
      unusedCells,
      gameContinue: true,
      winner: '',
    });
  };

  const move = ({ unusedCells, ...gameState }) => {
    const getRandomValue = (max) => Math.floor(Math.random() * (max + 1));
    const randomIndex = getRandomValue(unusedCells.length - 1);
    const randomId = unusedCells[randomIndex];
    const filteredUnusedCells = unusedCells.filter((id) => id !== randomId);
    const cells = onMove(gameState, randomId);

    let gameContinue = gameState.gameContinue;
    const halfOfTheGame = Math.round(cells.length / 2);

    let winner = '';
    if (halfOfTheGame > filteredUnusedCells.length) {
      let humanScore = 0;
      let computerScore = 0;
      cells.forEach(({ clickedBy }) => {
        if (clickedBy === HUMAN) {
          humanScore++;
        } else if(clickedBy === COMPUTER) {
          computerScore++;
        }
      });
      if (humanScore >= halfOfTheGame) {
        winner = game.userName;
      } else if (computerScore >= halfOfTheGame) {
        winner = COMPUTER;
      }
      if (winner) {
        gameContinue = false;
        postWinner(winner, winners, setResponse);
      }
    }

    setGame({
      ...gameState,
      unusedCells: filteredUnusedCells,
      randomId,
      cells,
      gameContinue,
      winner,
    });
  };

  const onCellClick = (id) => {
    const cells = game.cells.map((cell) => {
      if (cell.id === id) {
        cell.clickedBy = HUMAN;
      }
      return cell;
    });

    setGame({
      ...game,
      cells,
    });
  };
  
  useEffect(() => {
    if (game.gameContinue) {
      const timerId = setTimeout(() => move(game), game.delay);
      return () => clearTimeout(timerId);
    }
  }, [game]);

  const { winner, userName, cells } = game;

  return (
    <div className="App">
      <Container>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Controls
              settings={settings}
              loading={settingsLoading}
              setGameMode={setGameMode}
              gameStarted={!!userName && (!winner || !!winner)}
              gameFinished={!!winner}
            />
          </Grid>
          <Grid item xs={8}>
            <Field cells={cells} onCellClick={onCellClick} winner={winner} />
          </Grid>
          <Grid item xs={4}>
            <LeaderBoard winners={winners} loading={winnersLoading} />
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export default App;
