import React from 'react';
import { CircularProgress } from '@material-ui/core';

const withLoader = (Component) => ({ loading, ...props }) => {
  if (loading) {
    return (
      <div className="spinner-wrap">
        <CircularProgress />
      </div>
    );
  }

  return (
    <Component {...props} />
  );
};

export default withLoader;