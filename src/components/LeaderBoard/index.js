import React from 'react';
import { List, ListItem } from '@material-ui/core';
import PropTypes from 'prop-types';
import withLoader from '../Hoc/withLoader';

function LeaderBoard({ winners }) {

  return (
    <List>
      {winners.map(({ id, winner, date }) => <ListItem key={id}>{winner} {date}</ListItem>)}
    </List>
  );
}

LeaderBoard.propTypes = {
  winners: PropTypes.arrayOf(PropTypes.object),
};

LeaderBoard.defaultProps = {
  winners: [],
};

export default withLoader(LeaderBoard);
