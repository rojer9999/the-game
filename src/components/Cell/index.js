import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { HUMAN } from '../../constants';

function Cell({ id, isActive, onCellClick, clickedBy }) {
  const [ clicked, wasClick ] = useState(false);
  const onFirstClick = (id) => {
    if (clicked || !isActive) {
      return;
    }
    wasClick(true);
    onCellClick(id);
  };

  const getClass = () => {
    if (clickedBy === HUMAN || clicked) {
      return 'human';
    } else if (isActive) {
      return 'active';
    } else if (clickedBy) {
      return 'disabled';
    }
    return '';
  };

  return (
    <div className={"cell " + getClass()} onClick={() => onFirstClick(id)} />
  );
}

Cell.propTypes = {
  id: PropTypes.string,
  isActive: PropTypes.bool,
  onCellClick: PropTypes.func,
  clickedBy: PropTypes.string,
};

Cell.defaultProps = {
  id: '',
  isActive: false,
  onCellClick: () => {},
  clickedBy: '',
};

export default Cell;
